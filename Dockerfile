FROM node:latest

WORKDIR /usr/src/app

ENV APPLICATION_INSTANCE=example

COPY ./package.json ./package-lock.json* ./

RUN npm install

COPY . .

EXPOSE 8080

CMD [ "node", "src/count-server.js" ]