## Q1
Microservices architecture is a design approach in which an application is composed of independent services. Each service focuses on a specific functionality and communicates with other services over well-defined APIs. 
Monolithic application, where all functionalities are tightly integrated into a single codebase. Any change affects the entire system, whereas in microservices, changes are isolated to specific services.

## Q2
Microservices:
- Pros: Improved scalability, flexibility, and resilience. Independent deployment and development of services. Easier to adopt new technologies and processes.
- Cons: Increased complexity in managing multiple services. Communication overhead between services. 

Monolithic:
- Pros: Easier to develop and deploy, to debug and test. Initial setup and development can be faster.
- Cons: Scalability challenges. Limited flexibility in adopting new technologies. Large and complex codebase can slow down development.

## Q3
In Microservices architecture, the application should be split based on business capabilities or functionalities. Each microservice handles a specific business function and operates independently. This approach enables easier maintenance, and scalability, as each service can be scaled independently based on demand. Each service can also be developed by one team or a small group of teams, allowing for faster development and deployment.

## Q4
The CAP theorem states that a distributed system cannot simultaneously guarantee Consistency, Availability, and Partition tolerance. In the context of microservices and distributed systems, this implies trade-offs. For instance, during network failures, a system must choose between consistency and availability.

## Q5
The consequences of the CAP theorem on architecture include:
- Need to make trade-offs based on business requirements.
- Designing services to handle network partitions and failures.
- Deciding between strong consistency and high availability based on use cases.

## Q6
In a cloud environment, microservices can enhance scalability by allowing each service to scale independently. For example, for a ecommerce application, the product catalog service can be scaled up during a sale, while the payment service can be scaled up during checkout. This approach allows for better resource utilization and cost efficiency.

## Q7
Statelessness means that the service does not store any user session data internally. In microservices, statelessness is crucial as it allows services to be easily scalable and redeployed, as there's no session data tied to specific instances, enabling load balancing and flexibility in managing service instances.

## Q8
An API Gateway serves as an intermediary layer that handles incoming requests to various microservices. It's included in the microservices architecture to provide a single entry point for all clients, and to handle cross-cutting concerns such as authentication, rate limiting, and caching.
